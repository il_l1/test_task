install_gitlab_runner
=========
Ansible роль для настройки и запуска GitLab runner с shell executor.

В процессе применения роли устанавливаются все необходимые компоненты для работы раннера и проверки синтаксиса YAML, shell и Dockerfile:
- Docker
- hadolint
- yamllint
- shellcheck

Протестировано для запуска в Debian 10
